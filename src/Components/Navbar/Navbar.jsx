// import React from 'react'
// import {Link} from 'react-router-dom';

// import { useState } from "react"

// import "./Navbar.css";


// const Nav = () => {
//   const [isNavExpanded, setIsNavExpanded] = useState(false)
// return (

//   // <div>Navbar</div>
//   <nav className="navigation" id='position'>
//     <a href="/" className="brand-name">
//      Swachh Bharat
//     </a>
//     <button className="hamburger" onClick={() => {
//         setIsNavExpanded(!isNavExpanded)
//       }}>
//       {/* icon from heroicons.com */}
//       <svg
//         xmlns="http://www.w3.org/2000/svg"
//         className="h-5 w-5"
//         viewBox="0 0 20 20"
//         fill="white"
//       >
//         <path
//           fillRule="evenodd"
//           d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
//           clipRule="evenodd"
//         />
//       </svg>
//     </button>
//     <div className={isNavExpanded ? "navigation-menu expanded" : "navigation-menu"}>
//       <ul>
//         <li>
//           <Link to="/" className='nav-link' title=''>Home</Link>
//         </li>
//          <li>
//           <Link to="/concern" className='nav-link' title='qwerty'>Concern</Link>
//         </li> 
//         <li>
//           <Link to="/contactus" className='nav-link'>ContactUs</Link>
//         </li>
//         <li>
//           <Link to="/initiative" className='nav-link'>Initiative</Link>
//         </li>
//         <li>
//           <Link to="/collaboration" className='nav-link'>Collaboration</Link>
//         </li>

//       </ul>
//     </div>
//   </nav>
// )
// }

// export default Nav


//import React from 'react'
import React from 'react'
import { Link } from 'react-router-dom';

import { useState } from "react"

const Navbar = () => {
  return (
    <div>
      <div class="wrap">
        <div class="container">
          <div class="row justify-content-between">
            <div class="col-12 col-md d-flex align-items-center">
              <p class="mb-0 phone"><span class="mailus">Phone no:</span> <a href="#">+00 1234 567</a> or <span class="mailus">email us:</span> <a href="#">emailsample@email.com</a></p>
            </div>
            <div class="col-12 col-md d-flex justify-content-md-end">
              <div class="social-media">
                <p class="mb-0 d-flex">
                  <a href="#" class="d-flex align-items-center justify-content-center"><span className="fa fa-facebook"><i className="sr-only">Facebook</i></span></a>
                  <a href="#" class="d-flex align-items-center justify-content-center"><span className="fa fa-twitter"><i className="sr-only">Twitter</i></span></a>
                  <a href="#" class="d-flex align-items-center justify-content-center"><span className="fa fa-instagram"><i className="sr-only">Instagram</i></span></a>
                  <a href="#" class="d-flex align-items-center justify-content-center"><span className="fa fa-dribbble"><i className="sr-only">Dribbble</i></span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <nav className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light nav-bg" id="ftco-navbar">
        <div className="container">
         <Link to="/"> <a className="navbar-brand">Swachh<span>Bharat</span></a></Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="fa fa-bars"></span> Menu
          </button>
          <div className="collapse navbar-collapse" id="ftco-nav">
            <ul className="navbar-nav ml-auto">
              <li >
                <Link to="/" className="nav-item active"><a href="" className="nav-link">Home</a></Link>


              </li>
              <li >
                <Link to="/concern" className="nav-item">
                  <a href="" className="nav-link">Concern</a>
                </Link>

              </li>
              <li >
                <Link to="/contactus" className="nav-item">
                  <a href="" className="nav-link">Contact Us</a>
                </Link>

              </li>
              <li >
                <Link to="/initiative" className="nav-item">
                  <a href="" className="nav-link">Initiative</a>
                </Link>

              </li>
              <li >
                <Link to="/collaboration" className="nav-item">
                  <a href="" class="nav-link">Collaboration</a>
                </Link>

              </li>
              <li >
                <Link to="/dynamic" className="nav-item">
                  <a href="" class="nav-link">Dynamic Image</a>
                </Link>

              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Navbar