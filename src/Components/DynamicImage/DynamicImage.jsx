


import React, { useState, useEffect } from 'react';
import nsobanner from './../../images/nsobanner.jpg';

const App = () => {
  const [imageUrl, setImageUrl] = useState(null);
  const [externalImageUrl, setExternalImageUrl] = useState(null);
  const [loading, setLoading] = useState(true);
  const [fetchedImageStyle, setFetchedImageStyle] = useState({
    position: 'relative',
    top: '-36%',
    left: '20%',
    transform: 'translate(-328%, -97%)',
    height: "150px",
    width: "150px"
  });
  const imageDescription = 'A dynamically fetched image';

  useEffect(() => {
    const url = 'https://cors-anywhere.herokuapp.com/https://testingnso.sttar.in/profile/1715747582.jpg';
    setImageUrl(url);

    const externalUrl = nsobanner; // Local image URL
    setExternalImageUrl(externalUrl);
    setLoading(false);
  }, []);

  const handleDownload = async () => {
    if (imageUrl && externalImageUrl) {
      setLoading(true);
      try {
        const [response1, response2] = await Promise.all([
          fetch(imageUrl),
          fetch(externalImageUrl)
        ]);
        const [blob1, blob2] = await Promise.all([
          response1.blob(),
          response2.blob()
        ]);

        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        const bannerImage = new Image();
        const fetchedImage = new Image();

        bannerImage.src = URL.createObjectURL(blob2);
        fetchedImage.src = URL.createObjectURL(blob1);

        bannerImage.onload = () => {
          canvas.width = bannerImage.width;
          canvas.height = bannerImage.height;
          ctx.drawImage(bannerImage, 0, 0);

          fetchedImage.onload = () => {
            const maxWidth = 150; // Max width for the fetched image
            const maxHeight = 150; // Max height for the fetched image

            const aspectRatio = fetchedImage.width / fetchedImage.height;
            let newWidth = fetchedImage.width;
            let newHeight = fetchedImage.height;

            if (newWidth > maxWidth) {
              newWidth = maxWidth;
              newHeight = newWidth / aspectRatio;
            }
            if (newHeight > maxHeight) {
              newHeight = maxHeight;
              newWidth = newHeight * aspectRatio;
            }

            const x = (canvas.width - newWidth) / 2; // Center the fetched image horizontally
            const y = (canvas.height - newHeight) / 2; // Center the fetched image vertically

            ctx.drawImage(fetchedImage, x, y, newWidth, newHeight);

            canvas.toBlob(blob => {
              const url = window.URL.createObjectURL(blob);
              const link = document.createElement('a');
              link.href = url;
              link.setAttribute('download', 'combinedImage.jpg');
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
              setLoading(false);

              // Clean up
              URL.revokeObjectURL(bannerImage.src);
              URL.revokeObjectURL(fetchedImage.src);

              // Adjust position after download
              setFetchedImageStyle({
                top: '-36%',
                left: '20%',
                transform: 'translate(-328%, -97%)',

                height: "150px",
                width: "150px"
              });
            }, 'image/jpeg');
          };
        };
      } catch (error) {
        console.error('Error downloading images:', error);
        setLoading(false);
      }
    }
  };

  return (
    <div style={{ marginTop: "220px" }}>
      <h1>My Image Fetching App</h1>
      {loading ? (
        <p>Loading...</p>
      ) : (
        imageUrl && externalImageUrl && (
          <>
            <div style={{ position: 'relative' }}>
              <img src={externalImageUrl} alt="External Image" className='banner-image' />
              {/* Adjusted style for fetched image */}
              <img src={imageUrl} alt={imageDescription} className='fetched-image' style={fetchedImageStyle} />
            </div>
            <div style={{ marginTop: "20px" }}>
              <button onClick={handleDownload}>Download Image</button>
            </div>
          </>
        )
      )}
    </div>
  );
};

export default App;

























